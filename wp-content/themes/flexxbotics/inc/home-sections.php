<?php
function get_video_section ($section) {
  // menu_title - text
  // text_above_menu - textarea
  // video - file
  // video_placeholder_image - image
  // autoplay - true/false
  ob_start();
    if ( isset($section['text_above_video']) ) { ?>
      <div class="content-above-video light-grey-bg pt-2">
        <div class="content">
          <div class="row justify-center">
            <div class="col-12 text-centered font-1 weight-400">
              <?php echo $section['text_above_video']; ?>
            </div>
          </div>
        </div>
      </div>
    <?php
    } ?>
    <div class="video-content-wrapper pb-2">
      <div class="content">
        <div class="video-container blue-border-top">
          <?php
          if ( $section['video_source'] == 'upload' && isset($section['video'] ) ) { ?>
            <video class="video-section-video" playsinline
              poster="<?php echo $section['video_placeholder_image']['sizes']['large']; ?>">
              <source src="<?php echo $section['video']['url']; ?>" type="video/mp4">
            </video>
            <div class="play-button-container">
              <div class="play-button">

              </div>
            </div>
          <?php
          }
          if ( $section['video_source'] == 'embed' && isset($section['video_embed']) ) { ?>
            <div class="video-embed-container">
              <?php echo $section['video_embed']; ?>
            </div>
          <?php
          } ?>
        </div>
      </div>
    </div>
  <?php
  return ob_get_clean();
}

function get_graphics_section ($section) {
  // graphic - image
  ob_start();
  if ( $graphics = $section['graphics'] ) { ?>
    <div class="content">
      <div class="row align-center py-2">
        <?php
        foreach ( $graphics as $graphic ) { ?>
          <div class="col-6">
            <div class="graphic">
              <img src="<?php echo $graphic['graphic']['sizes']['large']; ?>"/>
            </div>
          </div>
        <?php
        } ?>
      </div>
    </div>
  <?php
  }
  return ob_get_clean();
}

function get_info_block_section ($section) {
  // menu_title - text
  // header - text
  // content - textarea
  ob_start(); ?>
    <div class="content">
      <div class="content-inner text-centered px-2 py-2 white">
        <?php
        if ( $title = $section['menu_title'] ) { ?>
          <span class="section-title ibm"><?php echo $title; ?></span>
        <?php
        }
        if ( $header = $section['header'] ) { ?>
          <h2><?php echo $header; ?></h2>
        <?php
        }
        if ( $content = $section['content'] ) {
          echo $content;
        } ?>
      </div>
    </div>
  <?php
  return ob_get_clean();
}

function get_graphic_slider_section ($section) {
  // menu_title - text
  // description - textarea
  // slides - repeater
    // title - text
    // image - image
  ob_start(); ?>
    <div class="content i-v">
      <div class="row">
        <div class="col-4">
          <?php
          if ( $title = $section['menu_title'] ) { ?>
            <span class="section-title ibm"><?php echo $title; ?></span>
          <?php
          }
          if ( $description = $section['description'] ) { ?>
            <div class="section-description tk-monotype-grotesque-display-c uppercase font-1">
              <?php echo $description; ?>
            </div>
          <?php
          }
          if ( isset($section['slides']) ) { ?>
            <div class="graphic-slides-nav"></div>
          <?php
          } ?>
        </div>
        <div class="col-8">
          <?php
          if ( $slides = $section['slides'] ) { ?>
            <div class="graphic-slides" data-autoplay="<?php echo $section['slider_autoplay'] ? 'true' : 'false'; ?>" data-fade-time="<?php echo $section['slider_fade_time']; ?>" data-slide-time="<?php echo $section['time_between_slides']; ?>">
              <?php
              foreach ( $slides as $slide ) { ?>
                <div class="graphic-slide-wrapper">
                  <div class="graphic-slide-container">
                    <div class="graphic-slide flex justify-center align-center" data-title="<?php echo $slide['title']; ?>">
                      <?php echo file_get_contents( $slide['image']['sizes']['large']); ?>
                    </div>
                  </div>
                </div>
              <?php
              } ?>
            </div>
          <?php
          } ?>
        </div>
      </div>
    </div>
  <?php
  return ob_get_clean();
}

function get_info_boxes_section ($section) {
  // menu_title - text
  // header - text
  // content - textarea
  ob_start(); ?>
    <div class="content">
      <div class="row row-1-5">
        <?php
        if ( $title = $section['title'] ) { ?>
          <h2 class="col-12 font4 black mt-0 tk-termina"><?php echo $title; ?></h2>
        <?php
        }
        if ( $description = $section['description'] ) { ?>
          <div class="col-6 info-boxes-description font1 weight-600">
            <?php echo $description; ?>
          </div>
        <?php
        }
        if ( $boxes = $section['info_boxes'] ) { ?>
          <div class="col-12">
            <div class="info-boxes row pt-1">
              <?php
              foreach ( $boxes as $key => $box ) { ?>
                <div class="info-box col-6 col-6-lg-down col-12-md-down">
                  <div class="white-bg ratio-3-2">
                    <div class="flex align-center justify-center px-2 text-centered font1 weight-400">
                      <span class="info-box-number dark-grey flex align-center justify-center weight-700 font1">
                        <?php echo $key + 1; ?>
                      </span>
                      <?php echo $box['info_box']; ?>
                    </div>
                  </div>
                </div>
                <?php
              } ?>
            </div>
          </div>
        <?php
        }
        ?>
      </div>
    </div>
  <?php
  return ob_get_clean();
}
?>
