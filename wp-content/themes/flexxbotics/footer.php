<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Flexxbotics
 */

?>

	</div><!-- #content -->
	<div id="fb-contact-box">
		<div id="fb-contact-box-content">
			<?php echo do_shortcode(get_field('contact_form', 'option')); ?>
			<a href="#" class="close-contact">
				<?php echo file_get_contents( get_template_directory_uri() . '/images/graphics/close-button.svg'); ?>
			</a>
		</div>
	</div>

	<footer id="colophon" class="site-footer pt-2 pb-3 light-grey-bg blue-border-bottom">
		<div class="content">
			<?php
			if ( $footer_content = get_field('footer_content', 'option') ) { ?>
				<div class="row justify-center text-centered">
					<div id="footer-content" class="col-8 font2 weight-400 uppercase tk-monotype-grotesque-condensed">
						<?php echo $footer_content; ?>
					</div>
				</div>
			<?php
			} ?>
			<div class="row justify-center text-centered">
				<div class="site-title col-4 pt-1">
					<?php the_custom_logo(); ?>
				</div>
			</div>
		</div>
		<?php
		if ( $corner_image = get_field('corner_image_footer', 'option') ) {
			echo get_field('corner_image_link', 'option')
				? '<a class="ur-cert-badge" target="_blank" href="' . get_field('corner_image_link', 'option')  . '">'
				: '<div class="ur-cert-badge">'; ?>
				<img src="<?php echo $corner_image['sizes']['medium']; ?>" alt="<?php echo $corner_image['alt']; ?>"/>
			<?php
			echo get_field('corner_image_link', 'option') ? '</a>' : '</div>';
		} ?>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6256006.js"></script>
<!-- End of HubSpot Embed Code -->
</body>
</html>
