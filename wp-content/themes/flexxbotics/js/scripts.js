import $ from 'jquery';
import 'slick-carousel';

// Show/hide contact form
$('#contact-fb').on('click', function(e) {
  e.preventDefault()
  $('#fb-contact-box').toggleClass('active')
  if ( $('#fb-resources-container').hasClass('active') ) {
    $('#fb-resources-container').removeClass('active')
  }
  if ( $('#fb-contact-box').hasClass('active') ) {
    activateHeader();
  } else {
    deactivateHeader();
  }
})

$('#fb-contact-box').on('click', function(e) {
  // e.preventDefault()
  console.log(e.target)
  if (e.target !== this)
    return
  $('#fb-contact-box').toggleClass('active')
  deactivateHeader()
})

$('.close-contact').on('click', function(e) {
  e.preventDefault()
  $('#fb-contact-box').toggleClass('active')
  deactivateHeader()
})

if ( $('.play-button-container').length ) {
  $('.play-button-container').each( (i, item) => {
    $(item).on('click', () => {
      const video = $(item).parent().find('video');
      $(item).addClass('hidden');
      video[0].play();
      video[0].setAttribute("controls","controls")
    })
  })
}

if ( $('.resources-button').length ) {
  $('.resources-button').on('click', function(e) {
    e.preventDefault();
    $('#fb-resources-container').toggleClass('active')
    if ( $('#fb-contact-box').hasClass('active') ) {
      $('#fb-contact-box').removeClass('active')
    }
    if ( $('#fb-resources-container').hasClass('active') ) {
      activateHeader();
    } else {
      deactivateHeader();
    }
  })
  $('.close-resources a').on('click', function(e) {
    e.preventDefault();
    $('#fb-resources-container').toggleClass('active')
    deactivateHeader();
  })
}

function deactivateHeader() {
  if ( $('#header-content').hasClass('active') ) {
    $('#header-content').removeClass('active')
  }
}

function activateHeader() {
  if ( !$('#header-content').hasClass('active') ) {
    $('#header-content').addClass('active')
  }
}

$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top + ($(window).height() / 4);
  var elementBottom = elementTop + $(this).outerHeight();
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};

if ( $('.graphic_slider-wrapper').length ) {
  $('.graphic_slider-wrapper').each(function() {
    let $slider = $('.graphic-slides', this)
    let fadeTime = $($slider).data('fade-time')
    let slideTime = $($slider).data('slide-time')
    let autoPlay = $($slider).data('autoplay')
    let slideSettings = {
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: fadeTime * 1000,
      arrows: false,
      dots: true,
      fade: true,
      appendDots: $('.graphic-slides-nav', this),
      customPaging: function( slider, i ) {
        return `
        <div class="graphic-slide-label">
          <span class="font-1 weight-400">${$('.graphic-slide', slider.$slides[i]).data('title')}</span>
        </div>`;
      },
    }
    if ( autoPlay ) {
      slideSettings.autoplay = true
      slideSettings.autoplaySpeed = slideTime * 1000
      slideSettings.pauseOnHover = false
    }

    $($slider).slick(slideSettings).on('init', function(event, slick){
      if ( !$($slider).isInViewport() ) {
        $($slider).slickPause()
      }
      $(window).on('scroll', function() {
        let $slider = $('.graphic-slides', this)
        if ( $($slider).isInViewport() ) {
          if ( $($slider).data('autoplay') ) {
            $($slider).slickPlay()
          }
        } else {
          $($slider).slickPause()
        }
      });
    });
  })



}
