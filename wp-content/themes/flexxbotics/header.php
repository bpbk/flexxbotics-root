<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Flexxbotics
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/gve1rid.css">
	<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,400&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-147235035-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-147235035-1');
	</script>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header">
		<div id="header-content" class="flex align-center">
			<div class="content full-width">
				<div id="header-content-inner" class="flex align-center justify-space-between">
					<div class="header-mobile flex justify-space-between align-center">
						<div class="site-branding">
							<div class="site-title">
								<?php the_custom_logo(); ?>
							</div>
						</div><!-- .site-branding -->
						<a class="resources-button blue-bg white" href="#">
							resources
						</a>
					</div>

					<nav id="site-navigation" class="main-navigation tk-termina flex align-center">
						<?php
						if ( $sections = get_field('content_sections', get_option('page_on_front')) ) {
							foreach( $sections as $section ) {
								if ( isset($section['menu_title']) ) { ?>
									<a href="<?php echo is_home() ? '#' . slugify($section['menu_title']) : home_url() . '/#' . slugify($section['menu_title']); ?>">
										<?php echo $section['menu_title']; ?>
									</a>
									<?php
								}
							}
						} ?>
						<a href="#" id="contact-fb">contact</a>
						<a class="resources-button blue-bg white" href="#">
							resources
						</a>
					</nav><!-- #site-navigation -->
				</div>
			</div>
			<div class="blue-border"></div>
		</div>
		<div id="fb-resources-container">
			<div class="content full-width">
				<div class="row">
					<div class="col-3">
						<img id="robot-graphic" src="<?php echo get_template_directory_uri() . '/images/graphics/robot_illustration.svg'; ?>" alt="robot illustration"/>
					</div>
					<div id="fb-resources" class="col-9">
						<div class="row justify-end">
							<?php
							if ( $resources = get_field('fb_resources', 'option') ) {
								foreach ( $resources as $resource ) { ?>
									<div class="col-6-md-down fb-resource flex align-center">
										<div class="fb-resource-content">
											<a class="flex align-center" target="_blank" href="<?php echo $resource['file']['url']; ?>" alt="<?php echo $resource['file_name']; ?>">
												<img src="<?php echo get_template_directory_uri() . '/images/graphics/resource.svg'; ?>" alt="pdf icon"/>
												<span class="weight-400"><?php echo $resource['file_name']; ?></span>
											</a>
										</div>
									</div>
								<?php
								}
							} ?>
						</div>
						<div class="close-resources">
							<a href="#">close</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
