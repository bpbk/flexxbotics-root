<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Flexxbotics
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :
			/* Start the Loop */
			while ( have_posts() ) :
				the_post(); ?>
				<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<div id="hero-wrapper" class="light-grey-bg pt-1">
							<div class="content full-width">
								<div class="row">
									<div class="col-12">
										<div id="hero-container" class="white bg-centered border-radius-3 relative" style="background-image:url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large'); ?>);">
											<div class="bg-cover"></div>
											<div class="row flex align-center">
												<div id="hero-content-container" class="col-7">
													<div id="hero-content" class="tk-monotype-grotesque-condensed font2 weight-400">
														<?php the_content(); ?>
													</div>
												</div>
												<div class="col-5 py-1 our-value-wrapper">
													<div class="our-value-container">
														<div class="our-value px-1 py-1 font1 flex flex-column justify-center">
															<span class="section-title ibm">Our Value</span>
															<?php the_field('our_value'); ?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
						if ( $sections = get_field('content_sections', get_option('page_on_front')) ) {
	            foreach( $sections as $section ) {
								if ( isset($section['menu_title']) ) { ?>
									<a name="<?php echo slugify($section['menu_title']); ?>"></a>
								<?php
								} ?>
	              <section class="<?php echo $section['acf_fc_layout']; ?>-wrapper homepage-section">
	                <?php echo call_user_func('get_' . $section['acf_fc_layout'] . '_section', $section); ?>
	              </section>
	            <?php
	            }
	          }
					 	?>
					</main><!-- #main -->
				</div><!-- #primary -->
			<?php
			endwhile;
		endif;
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
